package com.upch.classi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.model.people.Person;

public class CourseActivity extends Activity implements OnClickListener,ConnectionCallbacks, OnConnectionFailedListener {
	private PlusClient mPlusClient;
	String url ="";
	private Person muser;
	JSONObject course;
	private static final String TAG = "CourseActivity";
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course);
		url = getIntent().getStringExtra("url");
		findViewById(R.id.view_in_web_button).setOnClickListener(this);
		findViewById(R.id.subscribe_button).setOnClickListener(this);
		/* Obtenemos los cursos de las personas */
		new GetCursoTask().execute(url);
		
		mPlusClient = new PlusClient.Builder(this, this, this).setActions(
		        "http://schemas.google.com/AddActivity","http://schemas.google.com/BuyActivity")
		        .setScopes(Scopes.PLUS_LOGIN,Scopes.PLUS_ME) // Space separated list of scopes
		        .build();
		
	}
	protected void onStart(){
		super.onStart();
		mPlusClient.connect();
	}
	protected void onStop(){
		super.onStop();
		mPlusClient.disconnect();
	}
	
	private String POST(String url, JSONObject json){
		String result = "";
        InputStream inputStream = null;
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            StringEntity se = new StringEntity(json.toString());
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
 
        } catch (Exception e) {
        	e.printStackTrace();
        }
		return result;
	}
	private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
 
        inputStream.close();
        return result;
 
    }
	public class GetCursoTask extends AsyncTask<String, Void, String>{
		/* Peticion POST al servidor para obtener el curso seleccionado en el ListItem anterior */
		@Override
		protected String doInBackground(String... urls) {
			JSONObject jsonObjSend = new JSONObject();
			
			try {
				jsonObjSend.put("title",urls[0]);
				JSONObject header = new JSONObject();
				header.put("deviceType","Android"); // Device type
				header.put("deviceVersion","2.0"); // Device OS version
				header.put("language", "es-es");	// Language of the Android client
				jsonObjSend.put("header", header);

				// Output the JSON object we're sending to Logcat:
				Log.i(TAG, jsonObjSend.toString(2));

			} catch (JSONException e) {
				e.printStackTrace();
			}
			String response = POST("http://moocs.codigofacilito.com/course_by_name",jsonObjSend);
			
			return response;
			
		}
		@Override
		protected void onPostExecute(String result){
			Bitmap mIcon_val;
			/* Modificamos la vista con la info de los cursos */
			try{
				course = new JSONObject(result);
				TextView course_title = (TextView) findViewById(R.id.course_title);
	    		course_title.setText(course.getString("title"));
        		TextView course_desc = (TextView) findViewById(R.id.course_description);
        		course_desc.setText(course.getString("description"));
        		Log.d("Img","http://moocs.codigofacilito.com/courses_imgs/"+course.getString("id")+"."+course.getString("extension"));
        		String newurl = "http://moocs.codigofacilito.com/courses_imgs/"+course.getString("id")+"."+course.getString("extension");
        		ImageView profile_photo = (ImageView) findViewById(R.id.course_image);
        		new LoadImagefromUrl().execute(profile_photo,newurl);
			}catch(Exception e){
				e.getStackTrace();
			}
			
		}
	}
	private class LoadImagefromUrl extends AsyncTask< Object, Void, Bitmap > {
        ImageView ivPreview = null;
        
        @Override
        protected Bitmap doInBackground( Object... params ) {
            this.ivPreview = (ImageView) params[0];
            String url = (String) params[1];
            return loadBitmap( url );
        }

        @Override
        protected void onPostExecute( Bitmap result ) {
            super.onPostExecute( result );
            ivPreview.setImageBitmap( result );
        }
    }

    public Bitmap loadBitmap( String url ) {
        URL newurl = null;
        Bitmap bitmap = null;
        try {
            newurl = new URL(url);
            bitmap = BitmapFactory.decodeStream( newurl.openConnection( ).getInputStream( ) );
        } catch ( MalformedURLException e ) {
            e.printStackTrace( );
        } catch ( IOException e ) {

            e.printStackTrace( );
        }
        return bitmap;
    }
	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.subscribe_button){
			new SaveCourse().execute();
		}
		if(view.getId() == R.id.view_in_web_button){
			Intent i = new Intent(Intent.ACTION_VIEW);
			Uri url;
			try{
				url = Uri.parse("http://moocs.codigofacilito.com/courses/"+course.getString("id"));
				i.setData(url);
				startActivityForResult(i, 0);
			}
			catch(Exception e){
				Toast.makeText(getApplicationContext(), "Cannot open the course in the web browser",Toast.LENGTH_LONG).show();
			}
			
		}
		
	}
	public class SaveCourse extends AsyncTask<PlusClient, Void, String> {
		private static final String TAG = "SaveToken";
		@Override
		protected String doInBackground(PlusClient... persons) {
			String token = null;
	        JSONObject jsonObjSend = new JSONObject();
	        SharedPreferences sharedPreferences = getSharedPreferences("classiPrefs",Context.MODE_PRIVATE);
        	String id = sharedPreferences.getString("userId","");
			try {
				jsonObjSend.put("id",muser.getId());
				jsonObjSend.put("course_id", course.getString("id"));

				// Add a nested JSONObject (e.g. for header information)
				JSONObject header = new JSONObject();
				header.put("deviceType","Android"); // Device type
				header.put("deviceVersion","2.0"); // Device OS version
				header.put("language", "es-es");	// Language of the Android client
				jsonObjSend.put("header", header);

				// Output the JSON object we're sending to Logcat:
				Log.i(TAG, jsonObjSend.toString(2));

			} catch (JSONException e) {
				e.printStackTrace();
			}
			String response = POST("http://moocs.codigofacilito.com/users/registerCourse",jsonObjSend);
			Log.i(TAG, response);
	        return response;
		}
		@Override
		protected void onPostExecute(String result){
			try{
				JSONObject js_o = new JSONObject(result);
				Toast.makeText(getApplicationContext(), "We've registered you to the course",Toast.LENGTH_LONG).show();
				Intent intent = new Intent(CourseActivity.this, WelcomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
				
			}catch(Exception e){
				e.getStackTrace();
			}
			
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Intent intent = new Intent(CourseActivity.this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		
	}
	@Override
	public void onConnected(Bundle connectionHint) {
		muser = mPlusClient.getCurrentPerson();
		
	}
	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}
}
