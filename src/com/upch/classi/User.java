package com.upch.classi;
/* Clase que representa al usuario */
public class User {
	private String username="Uriel";
	private String email = "";
	private int id = 0;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
