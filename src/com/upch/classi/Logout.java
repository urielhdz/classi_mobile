package com.upch.classi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.plus.PlusClient;

public class Logout extends Activity implements ConnectionCallbacks, OnConnectionFailedListener{
	private PlusClient mPlusClient;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 mPlusClient = new PlusClient.Builder(this, this, this).setActions(
			        "http://schemas.google.com/AddActivity","http://schemas.google.com/BuyActivity")
			        .setScopes(Scopes.PLUS_LOGIN,Scopes.PLUS_ME) // Space separated list of scopes
			        .build();
		 
	}
	protected void onStart(){
		super.onStart();
		mPlusClient.connect();
	}
	protected void onStop(){
		super.onStop();
		mPlusClient.disconnect();
	}
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Intent intent = new Intent(Logout.this,MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		
	}
	@Override
	public void onConnected(Bundle connectionHint) {
		/* Cierra sesion del usuario */
		if (mPlusClient.isConnected()) {
            mPlusClient.clearDefaultAccount();
            mPlusClient.disconnect();
            mPlusClient.connect();
        }
        Intent intent = new Intent(Logout.this,MainActivity.class);
		startActivity(intent);
		
	}
	@Override
	public void onDisconnected() {
		Intent intent = new Intent(Logout.this,MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(intent);
		
	}

}
