package com.upch.classi;

import java.util.Locale;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
/* Toda la logica para administrar los fragments segun los tabs */
public class SectionsPagerAdapter extends FragmentPagerAdapter{
	protected Context mContext;
	public SectionsPagerAdapter(Context cntx,FragmentManager fm) {
		super(fm);
		this.mContext = cntx;
	}

	@Override
	public Fragment getItem(int position) {
		/*Cambia el fragment dependiendo del tab seleccionado*/
		
		switch (position) {
			case 0:
				return new CoursesFragment();
			case 1:
				return new HomeworksFragment();
			case 2:
				return new AllCoursesFragment();
		}
		return null;
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return 3;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		/* Cambia el titutlo de la pagina dependiendo del Fragment */
		Locale l = Locale.getDefault();
		switch (position) {
		case 0:
			return mContext.getString(R.string.title_section1).toUpperCase(l);
		case 1:
			return mContext.getString(R.string.title_section2).toUpperCase(l);
		case 2:
			return mContext.getString(R.string.title_section3).toUpperCase(l);
		}
		return null;
	}

}
