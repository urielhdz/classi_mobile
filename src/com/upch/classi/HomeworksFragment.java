package com.upch.classi;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
/* Fragment para mostrar las tareas que el usuario debe entregar */
public class HomeworksFragment extends ListFragment{
	public static final String TAG = HomeworksFragment.class.getSimpleName();
	protected JSONArray json_data;
	protected ArrayList<HashMap<String,String>> materias_al = new ArrayList<HashMap<String,String>>();
	public static final String ARG_SECTION_NUMBER = "section_number";
	SharedPreferences sharedPreferences;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_homeworks,
				container, false);
		materias_al = new ArrayList<HashMap<String,String>>();
		GetCursosTask getData = new GetCursosTask();
        getData.execute();
		return rootView;
	}
	public void updateList() throws JSONException {
		if(json_data == null){
			// TODO: Handle error
		}
		else{
			HashMap<String,String> my_helper = new HashMap<String,String>();
			for (int i = 0; i < json_data.length(); i++) {
    			JSONObject jsonPost = json_data.getJSONObject(i);
    			String title = jsonPost.getString("titulo");
    			String slugg = jsonPost.getString("descripcion");
    			my_helper = new HashMap<String,String>();
    			my_helper.put("title", title);
    			my_helper.put("maestro", slugg);
    			this.materias_al.add(my_helper);
    		}
			String[] keys = {"title","maestro"};
			int[] ids = {android.R.id.text1,android.R.id.text2};
			SimpleAdapter adapter = new SimpleAdapter(getActivity(), this.materias_al, android.R.layout.simple_list_item_2,keys,ids);
			setListAdapter(adapter);
		}
		
	}
	private class GetCursosTask extends AsyncTask<Object, Void, JSONArray>{

		@Override
		protected JSONArray doInBackground(Object... arg0) {
			int responseCode = -1;
			
	        try {
	        	sharedPreferences = getActivity().getSharedPreferences("classiPrefs",Context.MODE_PRIVATE);
	        	String id = sharedPreferences.getString("userId","");
	        	URL jsonURL = new URL("http://moocs.codigofacilito.com/users/assessments/"+id);
	        	HttpURLConnection connection = (HttpURLConnection) jsonURL.openConnection();
	        	connection.connect();
	        	
	        	responseCode = connection.getResponseCode();
	        	if(responseCode == HttpURLConnection.HTTP_OK){
	        		InputStream inputStream = connection.getInputStream();
	        		Reader reader = new InputStreamReader(inputStream);
	        		char[] charArray = new char[10000];
	        		reader.read(charArray);
	        		String responseData = new String(charArray);
	        		JSONArray jsonPosts = new JSONArray(responseData);
	        		return jsonPosts;
	        	}
	        	else{
	        		Log.i(TAG, "Unsuccessful HTTP Response Code: " + responseCode);
	        	}
	        	Log.i(TAG, "Code: " + responseCode);
	        }
	        catch (MalformedURLException e) {
	        	Log.e(TAG, "Exception caught: ", e);
	        }
	        catch (IOException e) {
	        	Log.e(TAG, "Exception caught: ", e);
	        }
	        catch (Exception e) {
	        	Log.e(TAG, "Exception caught: ", e);
	        }
	        
	        return null;

		}
		@Override
		protected void onPostExecute(JSONArray result){
			json_data = result;
			Log.d("Homeworks response",json_data.toString());
			try {
				updateList();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
