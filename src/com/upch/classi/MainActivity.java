package com.upch.classi;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.model.people.Person;

public class MainActivity extends Activity implements OnClickListener,ConnectionCallbacks, OnConnectionFailedListener{

	private PlusClient mPlusClient;
	private ConnectionResult mResult;
	private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
	private static final String TAG = "MainActivity";
	private static final String URL = "http://localhost.com:8080";
	SharedPreferences sharedPreferences;
	private Person muser;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById(R.id.sign_in_button).setOnClickListener(this);
		/*Nos conectamos a Google Plus*/
		 mPlusClient = new PlusClient.Builder(this, this, this).setActions(
			        "http://schemas.google.com/AddActivity","http://schemas.google.com/BuyActivity")
			        .setScopes(Scopes.PLUS_LOGIN,Scopes.PLUS_ME) // Space separated list of scopes
			        .build();
		
		
	}
	protected void onStart(){
		super.onStart();
		mPlusClient.connect();
	}
	protected void onStop(){
		super.onStop();
		mPlusClient.disconnect();
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		/* Obtenemos la informacion de la persona y logueamos en el servidor */
		muser = mPlusClient.getCurrentPerson();
		
		findViewById(R.id.sign_in_button).setVisibility(View.GONE);
		
		findViewById(R.id.textView1).setVisibility(View.VISIBLE);
		new LoginTask().execute("http://moocs.codigofacilito.com/users/login.json");
		
		//new LoginTask().execute("192.168.1.123:3000/user/login.json");
	}
	public void onResume(){
		/* Crea el objeto para las sesiones */
		super.onResume();
		sharedPreferences=getSharedPreferences("classiPrefs",Context.MODE_PRIVATE);
	}
	private String POST(String url, JSONObject json){
		/* Envia peticion JSON al metodo post */
		String result = "";
        InputStream inputStream = null;
        try {
            DefaultHttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            StringEntity se = new StringEntity(json.toString());
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";
 
        } catch (Exception e) {
        	e.printStackTrace();
        }
		return result;
	}
	 private static String convertInputStreamToString(InputStream inputStream) throws IOException{
		 /* Convierte resultado de HTTP a String */
	        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	        String line = "";
	        String result = "";
	        while((line = bufferedReader.readLine()) != null)
	            result += line;
	 
	        inputStream.close();
	        return result;
	 
	    }  
	@Override
	public void onDisconnected() {
		
		
	}

	@Override
	public void onClick(View view) {
		if(view.getId() == R.id.sign_in_button && !mPlusClient.isConnected() && mResult != null){
			try{
				/* Abrimos la ventana de Login para que el usuario eliga cuenta y de permisos */
				mResult.startResolutionForResult(this,REQUEST_CODE_RESOLVE_ERR);
			}catch(SendIntentException e){
				mResult = null;
				mPlusClient.connect();
			}
		}
		
		
	}
	protected void onActivityResult(int requestCode, int responseCode, Intent intent){
		/* Nos conectamos a GPlus */
		if(requestCode == REQUEST_CODE_RESOLVE_ERR && responseCode == RESULT_OK){
			mResult = null;
			mPlusClient.connect();
		}
	}
	@Override
	public void onConnectionFailed(ConnectionResult result) {
		/* El usuario aun no se ha logueado */
		mResult = result;
		
	}
	private class LoginTask extends AsyncTask<String, Void, String>{
		/* Logueamos en el servidor con la info de Google Plus */
		@Override
		protected String doInBackground(String... urls) {
			JSONObject jsonObjSend = new JSONObject();
			
			try {
				// Add key/value pairs
				jsonObjSend.put("name", muser.getName().getGivenName());
				jsonObjSend.put("email", mPlusClient.getAccountName());
				jsonObjSend.put("id", muser.getId());

				// Add a nested JSONObject (e.g. for header information)
				JSONObject header = new JSONObject();
				header.put("deviceType","Android"); // Device type
				header.put("deviceVersion","2.0"); // Device OS version
				header.put("language", "es-es");	// Language of the Android client
				jsonObjSend.put("header", header);

				// Output the JSON object we're sending to Logcat:
				Log.i(TAG, jsonObjSend.toString(2));

			} catch (JSONException e) {
				e.printStackTrace();
			}

			// Send the HttpPostRequest and receive a JSONObject in return
			//JSONObject jsonObjRecv = HttpClient.SendHttpPost("http://localhost:3000/user/login", jsonObjSend);
			
			return POST(urls[0],jsonObjSend);
			

		}
		@Override
		protected void onPostExecute(String result){
			//Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
			
			JSONObject result_json;
			try {
				result_json = new JSONObject(result);
				Editor editor = sharedPreferences.edit();
				editor.putString("userId",result_json.getString("id"));
				editor.commit();
				//new SaveToken().execute();
				Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}
	public class SaveToken extends AsyncTask<PlusClient, Void, String> {
		private static final String TAG = "SaveToken";
		@Override
		protected String doInBackground(PlusClient... persons) {
			/* Guardamos el token que da Google en el Server */
			String token = null;
			
	        try {
	            token = GoogleAuthUtil.getToken(
	                    MainActivity.this,
	                    mPlusClient.getAccountName(),
	                    "oauth2:" + Scopes.PLUS_LOGIN);
	        } catch (IOException transientEx) {
	            // Network or server error, try later
	            Log.e(TAG, transientEx.toString());
	        } catch (UserRecoverableAuthException e) {
	            // Recover (with e.getIntent())
	            Log.e(TAG, e.toString());
	            Intent recover = e.getIntent();
	            startActivityForResult(recover, 0);
	        } catch (GoogleAuthException authEx) {
	            // The call is not ever expected to succeed
	            // assuming you have already verified that 
	            // Google Play services is installed.
	            Log.e(TAG, authEx.toString());
	        }
	        JSONObject jsonObjSend = new JSONObject();
			
			try {
				jsonObjSend.put("token",token);
				jsonObjSend.put("email", mPlusClient.getAccountName());
				jsonObjSend.put("id", muser.getId());

				// Add a nested JSONObject (e.g. for header information)
				JSONObject header = new JSONObject();
				header.put("deviceType","Android"); // Device type
				header.put("deviceVersion","2.0"); // Device OS version
				header.put("language", "es-es");	// Language of the Android client
				jsonObjSend.put("header", header);

				// Output the JSON object we're sending to Logcat:
				Log.i(TAG, jsonObjSend.toString(2));

			} catch (JSONException e) {
				e.printStackTrace();
			}
			String response = POST("http://moocs.codigofacilito.com/users/registerToken",jsonObjSend);
			Log.i(TAG, response);
	        return token;
	        
		}
		protected void onPostExecute(String result){
			Toast.makeText(getBaseContext(), mPlusClient.getAccountName(), Toast.LENGTH_LONG).show();
			
			
		}
	}


}